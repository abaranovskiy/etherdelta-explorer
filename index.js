const fs = require('fs');
const InputDataDecoder = require('ethereum-input-data-decoder');
const web3 = require('web3');
const async = require('async');
const elasticsearch = require('elasticsearch');
const BigNumber = require('bignumber.js');

const getDatabase = require('./utils/getDatabase');
const nextChunk = require('./utils/nextChunk');
const getTokenSymbol = require('./utils/getTokenSymbol');
const getWalletCreationDate = require('./utils/getWalletCreationDate');
const ensureWallet = require('./utils/ensureWallet');

const decoder = new InputDataDecoder(`${__dirname}/abi.json`);

const api = {};

const token_unique_traders = {};

// Коэффициенты комиссии биржи, могут меняться но пока используем фиксированные значения
const feeMake = '0';
const feeTake = '3000000000000000';

// Используем для проверки порядка обрабатываемых транзакций
let lastBlockNumber = 0;
let lastTransactionIndex = -1;

function onError(err) {
    console.error(err);
    process.exit(1);
}

function onSuccess() {
    process.exit(0);
}

function nextChunkCallback(err, transactions) {
    if (err) return onError(err);

    if (!transactions.length) {
        console.log('Все транзакции обработаны!');
        return onSuccess();
    }

    return processTransactions(transactions, function (err) {
        if (err) return onError(err);

        return nextChunk(nextChunkCallback);
    });
}

// Разогреваем все необходимые кэши и только после этого начинаем обработку
async.waterfall([

    // Создаем необходимые индексы
    function (next) {
        getDatabase(function (err, db) {
            if (err) return next(err);

            async.parallel([

                // Индекс на коллекции кошельков
                function (next2) {
                    console.log('Создаем индекс по адресам кошелька в statistic.wallets...');

                    const collection = db.collection('statistic.wallets');
                    collection.createIndex({wallet_address: 1}, {unique: true, background:true, w: 1}, function (err, indexName) {
                        if (err) return next2(err);

                        console.log(`Индекс ${indexName} успешно создан`);

                        return next2(null);
                    });
                },

                // Индекс на коллекции транзакций
                function (next2) {
                    console.log('Создаем индекс по обработанным транзакциям в transactions...');

                    const collection = db.collection('transactions');
                    collection.createIndex({_processed: 1}, {w: 1, background:true}, function (err, indexName) {
                        if (err) return next2(err);

                        console.log(`Индекс ${indexName} успешно создан`);

                        return next2(null);
                    });
                }
            ], function (err) {
                return next(err);
            });
        });
    },

    // Наполняем кэш первых трейдеров
    function (next) {
        getDatabase(function (err, db) {
            if (err) return next(err);

            const collection = db.collection('statistic.tokens_trades');

            collection.distinct('wallet_address', {}, {wallet_address: 1}, function (err, tokens_trades) {
                if (err) return next(err);

                // Сохраняем в кэш уникадбных трейдеров
                tokens_trades.forEach(function (tokens_trades) {
                    token_unique_traders[tokens_trades['wallet_address']] = true;
                });

                return next(null);
            });
        });
    }

], function (err) {
    if (err) return onError(err);

    // Start processing
    nextChunk(nextChunkCallback);
});

/**
 * @param transactions
 * @param callback
 */
function processTransactions(transactions, callback) {

    function processNext() {
        const transaction = transactions.shift();

        // Если транзакций больше нет, то выходим
        if (!transaction) {
            return process.nextTick(callback, null);
        }

        // Если номер блока меньше текущего, то где-то нарушили порядок
        if (Number(transaction.blockNumber) < lastBlockNumber) {
            return process.nextTick(callback, new Error(`Номер блока ${transaction.blockNumber} меньше текущего ${lastBlockNumber}`))
        }

        // Если номера блоков совпадают и индекс транзакции в блоке меньше текущей, то где-то нарушен порядок
        if (Number(transaction.blockNumber) === lastBlockNumber && Number(transaction.transactionIndex) < lastTransactionIndex) {
            return process.nextTick(callback, new Error(`Номер блока ${transaction.blockNumber} равен текущемк ${lastBlockNumber}, но индекс транзакции ${transaction.transactionIndex} меньше текущей ${lastTransactionIndex}`));
        }

        // Если номер блока транзакции совпадает с текущим и индекас транзакции совпадает с текущим, то где-то нарушен порядок
        if (Number(transaction.blockNumber) === lastBlockNumber && Number(transaction.transactionIndex) == lastTransactionIndex) {
            return process.nextTick(callback, new Error(`Номер блока ${transaction.blockNumber} и индекс текущей странзакции ${transaction.transactionIndex} совпадают`));
        }

        // Сбрасываем номер текущего блока и транзакции
        lastBlockNumber = Number(transaction.blockNumber);
        lastTransactionIndex = Number(transaction.transactionIndex);

        console.log(`Обрабатываем транзакцию: ${transaction.hash} на блоке ${transaction.blockNumber} и индекске ${transaction.transactionIndex}`);

        // Если транзакция с ошибкой, то переходим к следующей
        if (transaction.isError !== '0') {
            return process.nextTick(processNext);
        }

        // Декодируем input data
        transaction.input = decoder.decodeData(transaction.input);

        // Если не известно имя вызываемого метода, то переходим к следующей
        if (transaction.input.name == null) {
            return process.nextTick(processNext);
        }

        // Получаем имя функции обработчика
        let inputHandler = api[transaction.input.name];

        // Если обработчие не определен, то выходим
        if (typeof inputHandler !== 'function') {
            return callback(new Error('Метод "' + transaction.input.name + '" не реализован'));
        }

        console.time(transaction.input.name);

        // Вызываем обработчик
        inputHandler(transaction, function (err) {
            if (err) return callback(err);

            console.timeEnd(transaction.input.name);

            // Переходим к следующей транзакции
            return processNext();
        });
    }

    // Начинаем обрабатывать транзакции
    return processNext();
}

/**
 * @param transaction
 * @param callback
 */
api.deposit = function (transaction, callback) {
    var deposit_eth = +web3.utils.fromWei(transaction.value, 'ether');

    // Гарантируем наличие кошелька
    ensureWallet(transaction.from, function (err) {
        if (err) return callback(err);

        // Выходим как можно раньше, чтобы не ждать выаолнения запроса
        process.nextTick(callback, null);

        // Добавляем средства на кошелек
        getDatabase(function (err, db) {
            if (err) return callback(err);

            const collection = db.collection('statistic.wallets');

            // updateWallet(transaction.from, {$inc: {deposit_eth: deposit_eth}})

            collection.updateOne({wallet_address: transaction.from}, {$inc: {deposit_eth: deposit_eth}}, function (err, result) {
                if (err) return callback(err);

                if (result.result.n !== 1) {
                    return callback(new Error('Не удалось увеличить депозит для кошелька' + transaction.from));
                }
            });
        });
    });
};

/**
 * @param transaction
 * @param callback
 */
api.withdraw = function (transaction, callback) {
    var withdraw_eth = +web3.utils.fromWei(transaction.input.inputs[0], 'ether');

    // Гарантируем наличие кошелька
    ensureWallet(transaction.from, function (err) {
        if (err) return callback(err);

        // Выходим как можно раньше, чтобы не ждать выаолнения запроса
        process.nextTick(callback, null);

        // Добавляем средства на кошелек
        getDatabase(function (err, db) {
            if (err) return callback(err);

            const collection = db.collection('statistic.wallets');

            // updateWallet(transaction.from, {$inc: {withdraw_eth: withdraw_eth}})

            collection.updateOne({wallet_address: transaction.from}, {$inc: {withdraw_eth: withdraw_eth}}, function (err, result) {
                if (err) return callback(err);

                if (result.result.n !== 1) {
                    return callback(new Error('Не удалось увеличить возврат для кошелька' + transaction.from));
                }
            });
        });
    });
};

/**
 * @param transaction
 * @param callback
 */
api.trade = function (transaction, callback) {

    // Пропускаем транзакции в которых покупаемый и продаваемый токены равны
    if (transaction.input.inputs[0] === transaction.input.inputs[2]) {
        console.error(`Покупаемый и продаваемый токен совпадают, ${transaction.hash}`);
        return process.nextTick(callback, null);
    }

    // Проверяем является ли текущая сделка первой
    let wallet_is_first_trade = (token_unique_traders[transaction.from] !== true);

    // Запоминаем трейдера в кэше
    token_unique_traders[transaction.from] = true;

    async.waterfall([

        // Продажа эфира для покупки другого токена
        function (next) {

            if (transaction.input.inputs[0] !== '0000000000000000000000000000000000000000') {
                return next(null);
            }

            // Рассчитываем размер комиссии покупателя (taker) и продавца (maker)
            let feeMakeXfer = BigNumber(transaction.input.inputs[10]).multipliedBy(feeMake).dividedBy(web3.utils.toWei('1', 'ether'));
            let feeTakeXfer = BigNumber(transaction.input.inputs[10]).multipliedBy(feeTake).dividedBy(web3.utils.toWei('1', 'ether'));

            // Размер комиссии, которую получаем maker от taker
            let feeRebateXfer = 0;

            let exchangeFeeWei = BigNumber(feeMakeXfer).plus(feeTakeXfer).minus(feeRebateXfer);

            const token_trade = {
                // Продаваемый токен
                token                   : `0x${transaction.input.inputs[2]}`,
                // Время транзакции (сек)
                timestamp               : new Date(transaction.timeStamp * 1000).toISOString(),
                // Тип сделки
                trade_type              : 'buying',
                // Количество потраченного эфира
                trade_eth               : +web3.utils.fromWei(transaction.input.inputs[10], 'ether'),
                // Разаботок биржи на коммисии
                exchange_fee_eth        : +web3.utils.fromWei(exchangeFeeWei.toFixed(0), 'ether'),
                // Стоимость газа
                gas_price_eth           : +web3.utils.fromWei(transaction.gasPrice, 'ether'),
                // Используемый газ транзакции
                gas_used_eth            : +web3.utils.fromWei(transaction.gasUsed, 'ether'),
                // Адрес кошелька продавца
                wallet_address          : transaction.from,
                // Первая сделка для кошелька ?
                wallet_is_first_trade   : wallet_is_first_trade,
                // Хэш транзакции
                tx                      : transaction.hash
            };

            async.waterfall([

                // Добавляем символ токена в объект сделки
                function (next2) {

                    getTokenSymbol(token_trade['token'], function (err, token_symbol) {
                        if (err) return next2(err);

                        // Добавляем символ токена в объект
                        token_trade['token_symbol'] = token_symbol;

                        return next2(null);
                    });
                },

                // Добавляем дату создания кошелька
                function (next2) {

                    getWalletCreationDate(token_trade['wallet_address'], function (err, wallet_created_at) {
                        if (err) return next2(err);

                        // Добавляем адрес создания кошелька в объект
                        token_trade['wallet_created_at'] = new Date(wallet_created_at * 1000).toISOString();

                        return next2(null);
                    });
                },

                // Сохраняем в общий файл торгуемых токенов
                function (next2) {

                    // Выходим как можно раньше, чтобы не ждать запрос
                    process.nextTick(next2, null);

                    // Добавляем документ в коллекцию статистики
                    getDatabase(function (err, db) {
                        if (err) return next2(err);

                        const collection = db.collection('statistic.tokens_trades');

                        collection.insertOne(token_trade, function (err, result) {
                            if (err) return next2(err);

                            if (result.insertedCount !== 1) {
                                return next2(new Error('Не удалось вставить документ в статистику: ' + JSON.stringify(token_trade)));
                            }
                        });
                    });
                },

                // Для первой сделки кошелька записываем в файл первых покупателей токена
                function (next2) {

                    // Если для кошелька уже была сделка, то выходим
                    if (!wallet_is_first_trade) {
                        return next2(null);
                    }

                    async.waterfall([

                        // Записываем дату первой сделки кошелька
                        function (next3) {

                            // Гарантируем наличие кошелька
                            ensureWallet(transaction.from, function (err) {
                                if (err) return next3(err);

                                // Выходим как можно раньше, чтобы не ждать запрос
                                process.nextTick(next3, null);

                                getDatabase(function (err, db) {
                                    if (err) return next3(err);

                                    const collection = db.collection('statistic.wallets');

                                    // updateWallet(transaction.from, {$set: {start_trade_ts:  new Date(transaction.timeStamp * 1000).toISOString()}})

                                    collection.updateOne({wallet_address: transaction.from}, {$set: {start_trade_ts:  new Date(transaction.timeStamp * 1000).toISOString()}}, function (err, result) {
                                        if (err) return next3(err);

                                        if (result.result.n !== 1) {
                                            return next3(new Error('Не удалось установить дату первой сделки для кошелька' + transaction.from));
                                        }
                                    });
                                });
                            });
                        }

                    ], next2);
                },

                // Записываем дату последней сделки на бирже и добавляем тогруемый токен
                function (next2) {

                    // Гарантируем наличие кошелька
                    ensureWallet(transaction.from, function (err) {
                        if (err) return next2(err);

                        // Выходим как можно раньше, чтобы не ждать выполнение запроса
                        process.nextTick(next2, null);

                        // Добавляем торгуемый токен (если еще не добавлен)
                        getTokenSymbol(`0x${transaction.input.inputs[2]}`, function (err, token_symbol) {
                            if (err) return next2(err);

                            getDatabase(function (err, db) {
                                if (err) return next2(err);

                                const collection = db.collection('statistic.wallets');

                                // updateWallet(transaction.from, {
                                //     $addToSet: {trade_tokens: token_symbol},
                                //     $set: {end_trade_ts: new Date(transaction.timeStamp * 1000).toISOString()}
                                // });

                                collection.updateOne({wallet_address: transaction.from}, {
                                    $addToSet: {trade_tokens: token_symbol},
                                    $set: {end_trade_ts: new Date(transaction.timeStamp * 1000).toISOString()}
                                }, function (err, result) {
                                    if (err) return next2(err);

                                    if (result.result.n !== 1) {
                                        return next2(new Error('Не удалось добавить тогруемый токен и установить дату последней сделки кошелька' + transaction.from));
                                    }
                                });
                            });
                        });
                    });
                }
            ], next);
        },

        // Покупка эфира за другой токен
        function (next) {

            if (transaction.input.inputs[2] !== '0000000000000000000000000000000000000000') {
                return next(null);
            }

            // Рассчитываем стоимость одного токена в эфирах
            let priceOfTokenInEther = BigNumber(transaction.input.inputs[3]).dividedBy(transaction.input.inputs[1]);

            // Рассчитываем размер комиссии покупателя (taker) и продавца (maker)
            let feeMakeXfer = BigNumber(transaction.input.inputs[10]).multipliedBy(feeMake).dividedBy(web3.utils.toWei('1', 'ether')).multipliedBy(priceOfTokenInEther);
            let feeTakeXfer = BigNumber(transaction.input.inputs[10]).multipliedBy(feeTake).dividedBy(web3.utils.toWei('1', 'ether')).multipliedBy(priceOfTokenInEther);

            // Размер комиссии, которую получаем maker от taker
            let feeRebateXfer = 0;

            let tradeWei = BigNumber(priceOfTokenInEther).multipliedBy(transaction.input.inputs[10]);
            let exchangeFeeWei = BigNumber(feeMakeXfer).plus(feeTakeXfer).minus(feeRebateXfer);

            const token_trade = {
                // Продаваемый токен
                token                   : `0x${transaction.input.inputs[0]}`,
                // Время транзакции (сек)
                timestamp               : new Date(transaction.timeStamp * 1000).toISOString(),
                // Тип сделки
                trade_type              : 'selling',
                // Количество приобретенного эфира
                trade_eth               : +web3.utils.fromWei(tradeWei.toFixed(0), 'ether'),
                // Разаботок биржи на коммисии
                exchange_fee_eth        : +web3.utils.fromWei(exchangeFeeWei.toFixed(0), 'ether'),
                // Стоимость газа
                gas_price_eth           : +web3.utils.fromWei(transaction.gasPrice, 'ether'),
                // Используемый газ транзакции
                gas_used_eth            : +web3.utils.fromWei(transaction.gasUsed, 'ether'),
                // Адрес кошелька продавца
                wallet_address          : transaction.from,
                // Первая сделка для кошелька ?
                wallet_is_first_trade   : wallet_is_first_trade,
                // Хэш транзакции
                tx                      : transaction.hash
            };

            async.waterfall([

                // Добавляем символ токена в объект сделки
                function (next2) {

                    getTokenSymbol(token_trade['token'], function (err, token_symbol) {
                        if (err) return next2(err);

                        // Добавляем символ токена в объект
                        token_trade['token_symbol'] = token_symbol;

                        return next2(null);
                    });
                },

                // Добавляем дату создания кошелька
                function (next2) {

                    getWalletCreationDate(token_trade['wallet_address'], function (err, wallet_created_at) {
                        if (err) return next2(err);

                        // Добавляем адрес создания кошелька в объект
                        token_trade['wallet_created_at'] = new Date(wallet_created_at * 1000).toISOString();

                        return next2(null);
                    });
                },

                // Сохраняем в общий файл торгуемых токенов
                function (next2) {

                    // Переходим к следующей итерации, тобы не ждать запрос от базы
                    process.nextTick(next2, null);

                    // Добавляем документ в коллекцию статистики
                    getDatabase(function (err, db) {
                        if (err) return next2(err);

                        const collection = db.collection('statistic.tokens_trades');

                        collection.insertOne(token_trade, function (err, result) {
                            if (err) return next2(err);

                            if (result.insertedCount !== 1) {
                                return next2(new Error('Не удалось вставить документ в статистику: ' + JSON.stringify(token_trade)));
                            }
                        });
                    });
                },

                // Для первой сделки кошелька записываем в файл первых продавноц токена
                function (next2) {

                    // Если для кошелька уже была сделка, то выходим
                    if (!wallet_is_first_trade) {
                        return next2(null);
                    }

                    async.waterfall([

                        // Записываем дату первой сделки кошелька
                        function (next3) {

                            // Гарантируем наличие кошелька
                            ensureWallet(transaction.from, function (err) {
                                if (err) return next3(err);

                                // Выходим как можно раньше, чтобы не ждать запрос от БД
                                process.nextTick(next3, null);

                                getDatabase(function (err, db) {
                                    if (err) return next3(err);

                                    const collection = db.collection('statistic.wallets');

                                    // updateWallet(transaction.from, {
                                    //      {$set: {start_trade_ts:  new Date(transaction.timeStamp * 1000).toISOString()}}
                                    // });

                                    collection.updateOne({wallet_address: transaction.from}, {$set: {start_trade_ts:  new Date(transaction.timeStamp * 1000).toISOString()}}, function (err, result) {
                                        if (err) return next3(err);

                                        if (result.result.n !== 1) {
                                            return next3(new Error('Не удалось установить дату первой сделки для кошелька' + transaction.from));
                                        }
                                    });
                                });
                            });
                        }
                    ], next2);
                },

                // Записываем дату последней сделки на бирже
                function (next2) {

                    // Гарантируем наличие кошелька
                    ensureWallet(transaction.from, function (err) {
                        if (err) return next2(err);

                        // Переходим как можно раньше, чтобы не ждать запрос от БД
                        process.nextTick(next2, null);

                        // Добавляем торгуемый токен (если еще не добавлен) и добавлем дату последней сделки
                        getTokenSymbol(`0x${transaction.input.inputs[0]}`, function (err, token_symbol) {
                            if (err) return next2(err);

                            getDatabase(function (err, db) {
                                if (err) return next2(err);

                                const collection = db.collection('statistic.wallets');

                                // updateWallet(transaction.from, {
                                //     $addToSet: {trade_tokens: token_symbol},
                                //     $set: {end_trade_ts: new Date(transaction.timeStamp * 1000).toISOString()}
                                // });

                                collection.updateOne({wallet_address: transaction.from}, {
                                    $addToSet: {trade_tokens: token_symbol},
                                    $set: {end_trade_ts: new Date(transaction.timeStamp * 1000).toISOString()}
                                }, function (err, result) {
                                    if (err) return next2(err);

                                    if (result.result.n !== 1) {
                                        return next2(new Error('Не удалось добавить тогруемый токен и установить дату последней сделки кошелька' + transaction.from));
                                    }
                                });
                            });
                        });
                    });
                }

            ], next);
        }

    ], callback);
};

/**
 * @param transaction
 * @param callback
 */
api.order = function (transaction, callback) {
    return process.nextTick(callback, null);
};

/**
 * @param transaction
 * @param callback
 */
api.cancelOrder = function (transaction, callback) {
    return process.nextTick(callback, null);
};

/**
 * @param transaction
 * @param callback
 */
api.depositToken = function (transaction, callback) {
    return process.nextTick(callback, null);
};

/**
 * @param transaction
 * @param callback
 */
api.withdrawToken = function (transaction, callback) {
    return process.nextTick(callback, null);
};

api.balanceOf = function (transaction, callback) {
    return process.nextTick(callback, null);
};

api.testTrade = function (transaction, callback) {
    return process.nextTick(callback, null);
};

api.amountFilled = function (transaction, callback) {
    return process.nextTick(callback, null);
};

// Блок 4760899
// Был вызов метода "changeFeeAccount"