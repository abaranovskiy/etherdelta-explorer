/**
 * Скрипт для постраничного получения транзакций смарт контракта из blockchain etherium
 */

const request = require('request');
const querystring = require('querystring');
const getDatabase = require('../utils/getDatabase');

const ETHERSCAN_API_URL = 'https://api.etherscan.io/api';

getDatabase(function (err, db) {
     if (err) throw err;

     const collection = db.collection('transactions');

     function onError(err) {
         console.error(err);
         db.close();
         process.exit(1);
     }

     function onSuccess() {
         db.close();
         process.exit(0);
     }

     function nextChunk (page, offset) {

         const query = querystring.stringify({
             module:'account',
             action: 'txlist',
             address: '0x8d12a197cb00d4747a1fe03395095ce2a5cc6819',
             page: page,
             offset: offset,
             sort: 'asc',
             apikey:'YAKW4J1ZWBA3HPFQ4K79XDSH6WKV2WGMWI'
         });

         request.get({url: `${ETHERSCAN_API_URL}?${query}`, json: true}, function (error, response, body) {
             console.log(`Получена следующая порция транзакций; { page: ${page}, offset: ${offset} }`);

             if (error) {
                return onError(error);
             }

             if (!body.result) {
                 return onError(body.message);
             }

             if (!body.result.length) {
                 return onSuccess();
             }

             // Добавляем транзакции в базу
             return collection.insertMany(body.result, function (err) {
                 if (err) {
                     return onError(err);
                 }

                 nextChunk(page + 1, offset);
             });
         });
     }

     // Создаем индекс по идентификатору транзакции
     collection.createIndex({'hash': 1}, {unique: true}, function (err, result) {
         if (err) return onError(err);

         // Start processing
         nextChunk(1, 500);
     });
 });