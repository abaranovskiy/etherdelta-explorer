const request = require('request');
const fs = require('fs');
const path = require('path');

const ETHPLORER_API_URL = 'https://api.ethplorer.io';

// Путь к файлу с кэшем
const CACHE_FILE_PATH = path.join(__dirname, '..', '.cache', 'tokens_symbols.json');

const tokens_symbols = require(CACHE_FILE_PATH);

function unicodeToChar(text) {
    return text.replace(/\\u[\dA-F]{4}/gi,
        function (match) {
            return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
        });
}

module.exports = function (token_address, callback) {
    const token_symbol = tokens_symbols[token_address];

    // Если символ токена уже известен, то возвращаем
    if (token_symbol) {
        return process.nextTick(callback, null, token_symbol);
    }

    console.error('Получаем символ токета из API ethplorer.io ...');

    // Получаем из API ethplorer.io
    return request.get({url: `${ETHPLORER_API_URL}/getTokenInfo/${token_address}?apiKey=freekey`, timeout: 5000}, function (err, response, body) {
        // Если запрос выполнился с ошибкой, то повторяем через некоторое время
        if (err) {
            console.error(`Ошибка при выполнении запроса на получение символа токена: ${err}. Пробуем еще раз`);
            return setTimeout(module.exports.bind(null, token_address, callback), 1000);
        }

        // Проверяем ошибку вызова API
        if (body.error) {
            return callback(new Error(body.error.message));
        }

        const jsonBody = JSON.parse(unicodeToChar(body));

        // Проверяем alert на предмет изменения контракта токена
        if (jsonBody['alert'] && /^This contract is frozen. New (.*) contract is/.test(jsonBody['alert'])) {
            let match = jsonBody['alert'].match('<a href=\"/address/(.*)\">there</a>');
            if (match) {
                return module.exports(match[1], callback);
            }
        }

        let token_symbol = jsonBody['symbol'];

        // Если по каким-то причина не получили символ токена, то выходим с ошибкой
        if (!token_symbol) {
            // Сохраняем символ токена в кэш
            tokens_symbols[token_address] = token_address;
            console.error(`API не вернул символ токена: ${JSON.stringify(body)}`);

            // Возвращаем адрес
            return callback(null, token_address);
        }

        // Сохраняем символ токена в кэш
        tokens_symbols[token_address] = token_symbol;

        // Возврашаем результат
        callback(null, token_symbol);

        fs.writeFile(CACHE_FILE_PATH, JSON.stringify(tokens_symbols, null, 2), 'utf8', function (err) {
            if (err) return callback(err);
        });
    });
};