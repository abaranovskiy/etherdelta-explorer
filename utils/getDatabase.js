const MongoClient = require('mongodb').MongoClient;

let getDatabase = function (callback) {

    MongoClient.connect('mongodb://localhost:27017/etherdelta', function(err, db) {
        if (err) return callback(err);

        console.log("Connected successfully to server");

        getDatabase = function (callback) {
            process.nextTick(callback, null, db);
        };

        getDatabase(callback);
    });
};

module.exports = function (callback) {
    return getDatabase(callback);
};