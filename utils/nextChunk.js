const getDatabase = require('./getDatabase');

function nextChunk (callback) {

    // Запускаем процесс парсинга
    getDatabase(function (err, db) {
        if (err) return callback(err);

        const collection = db.collection('transactions');

        collection.find({_processed: {$ne: true}}, {limit: 5000}).toArray(function (err, transactions) {
            if (err) return callback(err);

            // Отмечаем транзакции как обработанные
            let ids = transactions.map(function (transaction) { return transaction._id});

            collection.updateMany({_id: {$in: ids}}, {$set: {_processed: true}}, function (err, result) {
                if (err) return callback(err);

                if (result.modifiedCount !== transactions.length) {
                    return callback(new Error('Не удалось отметить обновленные транзакции: ' + JSON.stringify(result)));
                }

                console.log(`Получена следующая порция транзакций: ${transactions.length} штук`);

                callback(null, transactions);
            });
        });
    });
}

module.exports = function (callback) {
    return nextChunk(callback);
};