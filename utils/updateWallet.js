const path = require('path');
const getWalletCreationDate = require('./getWalletCreationDate');

// Путь к файлу с кэшем
const WALLETS_FILE_PATH = path.join(__dirname, '..', 'wallets.json');

const wallets = require(WALLETS_FILE_PATH);

function syncWallets() {
    // Если с момента предыдущего вызова не прошло 5 секунд, то отклладываем синхронизацию
    if (syncWallets.lastExecutedAt && (Date.now() - syncWallets.lastExecutedAt) < 5000 ) {
        return;
    }

    // Обновляем время последней синхронизации
    syncWallets.lastExecutedAt = Date.now();

    // В противном случае перезаписываем файл с кошельками
    fs.writeFile(WALLETS_FILE_PATH, JSON.stringify(wallets, null, 2), 'utf8', function (err) {
        if (err) {
            console.error(err);
            return;
        }

        console.log('Файл с кошельками успешно пересохранен на диск');
    });
}

function ensureWallet(wallet_address, callback) {
    let wallet = wallets[wallet_address];
    // Если такой кошелек уже есть, то возвращаем
    if (wallet) {
        return process.nextTick(callback, wallet);
    }

    // Создаем и наполняем кошелек дефолтными данными
    getWalletCreationDate(function (err, wallet_created_at) {
        if (err) return callback(err);

        wallets[wallet_address] = {
            wallet_address: wallet_address,
            wallet_created_at: new Date(wallet_created_at * 1000).toISOString(),
            deposit_eth: 0,
            withdraw_eth: 0,
            trade_tokens: []
        };

        // Возвращаем созданные кошелек
        return callback(null, wallet);
    });
}

function updateWallet (wallet_address, update, callback) {

    // Получаем объект кошелька по адресу
    ensureWallet(wallet_address, function (err, wallet) {
        if (err) return callback(err);

        // Обновляем объект кошелька в зависимости от типа операции
        Object.keys(update).forEach(function (op) {
            switch (op) {

                // Увеличить значение на число
                case '$inc':
                    for (let field in update[op]) {
                        if (update[op].hasOwnProperty(field)) {
                            wallet[field] += update[op][field];
                        }
                    }
                break;

                // Установить новые значения свойств
                case '$set':
                    for (let field in update[op]) {
                        if (update[op].hasOwnProperty(field)) {
                            wallet[field] = update[op][field];
                        }
                    }
                break;

                // Добавить в массив, если еще нет
                case '$addToSet':
                    for (let field in update[op]) {
                        if (update[op].hasOwnProperty(field) && wallet[field].indexOf(update[op][field]) === -1) {
                            wallet[field].push(update[op][field]);
                        }
                    }
                break;
            }
        });

        // Возвращаем результат как можно раньше
        callback(null);

        // Записываем кэш в файл
        fs.writeFile(WALLETS_FILE_PATH, JSON.stringify(wallets, null, 2), 'utf8', function (err) {
            if (err) return callback(err);
        });
    });
}

module.exports = function (wallet_address, callback) {
    return updateWallet(wallet_address, callback);
};