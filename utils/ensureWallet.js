const getWalletCreationDate = require('./getWalletCreationDate');
const getDatabase = require('./getDatabase');

function populateCache (wallets_cache, callback) {

    getDatabase(function (err, db) {
        if (err) {
            console.error('Не удалось загрузить кэш кошельков');
            return;
        }

        const collection = db.collection('statistic.wallets');

        collection.find({}, {wallet_address: 1}).toArray(function (err, wallets) {
            if (err) {
                console.error('Не удалось загрузить кэш кошельков');
                return callback(null);
            }

            wallets.forEach(function (wallet) {
                wallets_cache[wallet['wallet_address']] = true;
            });

            return callback(null);
        });
    });
}

let ensureWallet = function (wallet_address, callback) {
    const wallets_cache = {};

    // Наполняем кэш данными из известных кошельков
    populateCache(wallets_cache, function (err) {
        if (err) callback(err);

        ensureWallet = function (wallet_address, callback) {
            // Если кошелек уже создан, то не базу не проверяем
            if (wallets_cache[wallet_address]) {
                return process.nextTick(callback, null);
            }

            console.error(`Кошелька ${wallet_address} нет в кэше`);

            getDatabase(function (err, db) {
                const collection = db.collection('statistic.wallets');

                getWalletCreationDate(wallet_address, function (err, wallet_created_at) {
                    if (err) return callback(err);

                    collection.insertOne({
                        wallet_address: wallet_address,
                        wallet_created_at: new Date(wallet_created_at * 1000).toISOString(),
                        deposit_eth: 0,
                        withdraw_eth: 0,
                        trade_tokens: []
                    },
                    function (err, result) {
                        if (err) return callback(err);

                        if (result.insertedCount !== 1) {
                            return callback(new Error(`Не удалось добавить новый кошелек с адресом ${wallet_address}`));
                        }

                        console.log(`Кошелек ${wallet_address} добавлен в базу`);

                        // Сохраняем кошелек в кэш
                        wallets_cache[wallet_address] = true;

                        // Выходим
                        return callback(null);
                    });
                });
            });
        };

        ensureWallet(wallet_address, callback);
    });
};

module.exports = function (wallet_address, callback) {
    return ensureWallet(wallet_address, callback);
};