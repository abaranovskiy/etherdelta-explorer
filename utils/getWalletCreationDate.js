const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const path = require('path');

// Адрес сайта для получения данных
const ETHERCHAIN_ORG_URL = 'https://www.etherchain.org';

// Путь к файлу с кэшем
const CACHE_FILE_PATH = path.join(__dirname, '..', '.cache', 'wallets_create_dates.json');

const wallets_creation_dates = require(CACHE_FILE_PATH);

module.exports = function (wallet_address, callback) {
    const wallet_creation_date = wallets_creation_dates[wallet_address];

    // Если дата создания кошелька уже узвестна, то возвращаем
    if (wallet_creation_date) {
        return process.nextTick(callback, null, wallet_creation_date);
    }

    console.error('Получаем дату создания кошелька из etherchain.org ...');

    // Парсим из сайта etherchain.org
    return request.get({url: `${ETHERCHAIN_ORG_URL}/account/${wallet_address}`, timeout: 5000}, function (err, response, body) {
        // Если запрос выполнился с ошибкой, то повторяем через некоторое время
        if (err) {
            console.error('Ошибка при выполнении запроса на получение даты создания кошелька. Пробуем еще раз');
            return setTimeout(module.exports.bind(null, wallet_address, callback), 1000);
        }

        const $ = cheerio.load(body, {
            xmlMode: true,
            normalizeWhitespace: true
        });

        const $icon = $('.fa-user-plus');

        // Если сервер упал, то пробуем немного позже
        if (/^The web server reported a bad gateway error./.test(body)) {
            console.error(`Сервер временно недоступен. Пробуем еще раз`);
            return setTimeout(module.exports.bind(null, wallet_address, callback), 1000);
        }

        // Если превышел лимит запросов к ресурсу, то продуем еще раз
        if (/^rate limit exceeded/.test(body)) {
            console.error(`Превышен лимит запросов к ресусру. ${body}. Пробуем еще раз`);
            return setTimeout(module.exports.bind(null, wallet_address, callback), 1000);
        }

        if (!/^First seen:/.test($icon.parent().text())) {
            return callback(new Error('Не удалось распарсить дату создания кошелька: ' + body));
        }

        const wallet_creation_date = Number($icon.next('[aria-ethereum-date]').attr('aria-ethereum-date'));

        if (isNaN(wallet_creation_date)) {
            return callback(new Error('Не удалось распарсить дату создания кошелька: ' + wallet_creation_date));
        }

        // Сохраняем дату в кэше
        wallets_creation_dates[wallet_address] = wallet_creation_date;

        // Возвращаем результат
        callback(null, wallet_creation_date);

        // Записываем кэш в файл
        fs.writeFile(CACHE_FILE_PATH, JSON.stringify(wallets_creation_dates, null, 2), 'utf8', function (err) {
            if (err) return callback(err);
        });
    });
};