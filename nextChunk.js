const MongoClient = require('mongodb').MongoClient;

let getCollection = function (callback) {
    let collection = null;

    MongoClient.connect('mongodb://localhost:27017/etherdelta', function(err, db) {
        if (err) return callback(err);

        // Сохраняем коллекцию к коллекцию
        collection = db.collection('transactions');

        getCollection = function (callback) {
            process.nextTick(callback, collection);
        };

        getCollection(callback);
    });
};

module.exports = function nextChunk (page, offset, callback) {

    return getCollection(function (collection) {
        // Рассчитываем offset и limit для получения данных из коллекции
        const skip = (page - 1) * offset;
        const limit = offset;

        collection.find({}).skip(skip).limit(limit).toArray(function (err, result) {
            return callback(err, result, {page: page, offset: offset});
        });
    });
};