const async = require('async');
const path = require('path');
const getDatabase = require('../utils/getDatabase');
const fs = require('fs');

// Путь к файлу с кэшем
const WALLETS_FILE_PATH = path.join(__dirname, '..', 'wallets.json');

// Путь к файлу со сделками
const TOKENS_TRADES_FILE_PATH = path.join(__dirname, '..', 'tokens_trades.json');

function onError(err) {
    console.error(err);
    process.exit(1);
}

function onSuccess() {
    console.log('Кошельки с новыми данными успешно сохранены на диске');
    process.exit(0);
}

getDatabase(function (err, db) {
    if (err) throw err;

    async.waterfall([

        // Получаем объект коллекций, необходимых для подсчета
        function (callback) {

            db.collection('statistic.wallets').find({}, {_id: 0}).toArray(function (err, wallets) {
                if (err) return callback(err);

                db.collection('statistic.tokens_trades').find({}, {_id: 0}).toArray(function (err, tokens_trades) {
                    if (err) return callback(err);

                    return callback(null, wallets, tokens_trades);
                });
            });
        },

        // Вычисляем дополнительные поля
        function (wallets, tokens_trades, callback) {

            async.eachSeries(tokens_trades, function (token_trade, nextEachCb) {
                console.log(`Обрабатываем транзакцию ${token_trade['tx']}`);

                async.waterfall([

                    // Получаем обхект кошелька по его адресу
                    function ensureWallet(next) {
                        const wallet = wallets.reduce(function (result, current) {
                            if (!result && current['wallet_address'] === token_trade['wallet_address']) {
                                result = current;
                            }

                            return result;
                        }, null);

                        if (!wallet) {
                            console.error(`Кошелька с адресом ${token_trade['wallet_address']} нет в коллекции`);
                            return process.nextTick(nextEachCb, null);
                        }

                        // Гарантируем наличие полей для подсчета акумулируемых значений
                        wallet['total_fee_eth'] = 0;
                        wallet['total_selling_eth'] = 0;
                        wallet['total_buying_eth'] = 0;

                        return process.nextTick(next, null, wallet);
                    },

                    // Добавляем суммарно заплаченную комиссию
                    function (wallet, next) {
                        wallet['total_fee_eth'] += token_trade['exchange_fee_eth'];

                        return process.nextTick(next, null, wallet);
                    },

                    // Добавляем первую сделку с токеном
                    function (wallet, next) {

                        // Если первой сделки не было, то выходим
                        if (token_trade['wallet_is_first_trade'] !== true || wallet.hasOwnProperty('first_trade_token')) {
                            return process.nextTick(next, null, wallet);
                        }

                        wallet['first_trade_token'] = token_trade['token_symbol'];
                        wallet['first_trade_token_at'] = token_trade['timestamp'];
                        wallet['first_trade_type'] = token_trade['trade_type'];
                        wallet['first_trade_eth'] = token_trade['trade_eth'];

                        return process.nextTick(next, null, wallet);
                    },

                    // Добавляем суммарный оборот продаж и покупок
                    function (wallet, next) {

                        // Для продаж
                        if (token_trade['trade_type'] === 'selling') {
                            wallet['total_selling_eth'] += token_trade['trade_eth'];
                            return process.nextTick(next, null, wallet);
                        }

                        // Для покупок
                        if (token_trade['trade_type'] === 'buying') {
                            wallet['total_buying_eth'] += token_trade['trade_eth'];
                            return process.nextTick(next, null, wallet);
                        }

                        // Кидаем ошибку, если оказались здесь
                        return next(new Error(`Не удалось определить тип сделки: ${JSON.stringify(token_trade)}`));
                    }
                ], nextEachCb);

            }, function (err) {
                return callback(err, wallets, tokens_trades);
            });
        }

    ], function (err, wallets, tokens_trades) {
        if (err) return onError(err);

        // Записываем результируеющий массив кошельков
        fs.writeFile(WALLETS_FILE_PATH, JSON.stringify(wallets, null, 2), 'utf8', function (err) {
            if (err) return onError(err);

            // Записываем массив сделок
            fs.writeFile(TOKENS_TRADES_FILE_PATH, JSON.stringify(tokens_trades, null, 2), 'utf8', function (err) {
                if (err) return onError(err);

                return onSuccess();
            });
        });
    });
});
