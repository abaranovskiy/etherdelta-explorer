const request = require('request');
const querystring = require('querystring');
const getDatabase = require('../utils/getDatabase');

const ETHERSCAN_API_URL = 'https://api.etherscan.io/api';

// Номер блока, с которого начинаются транзакции контракта
const FIRST_CONTRACT_BLOCK = 3154196;

// Количество блоков для получения в обном чанке
const CHUNK_BLOCK_OFFSET = 100;

getDatabase(function (err, db) {
    if (err) throw err;

    const collection = db.collection('transactions');

    function onError(err) {
        console.error(err);
        db.close();
        process.exit(1);
    }

    function onSuccess() {
        db.close();
        process.exit(0);
    }

    function nextChunk (startblock, offset) {

        const query = querystring.stringify({
            module:'account',
            action: 'txlist',
            address: '0x8d12a197cb00d4747a1fe03395095ce2a5cc6819',
            startblock: startblock,
            endblock: startblock + offset,
            sort: 'asc',
            apikey:'YAKW4J1ZWBA3HPFQ4K79XDSH6WKV2WGMWI'
        });

        request.get({url: `${ETHERSCAN_API_URL}?${query}`, json: true}, function (error, response, body) {
            console.log(`Получена следующая порция транзакций; { startblock: ${startblock}, offset: ${startblock + offset} }`);

            if (error) {
                return onError(error);
            }

            if (!body.result) {
                return onError(body.message);
            }

            if (!body.result.length) {
                return nextChunk(startblock + offset + 1, offset);
            }

            // Добавляем транзакции в базу
            return collection.insertMany(body.result, function (err) {
                if (err) {
                    return onError(err);
                }

                nextChunk(startblock + offset + 1, offset);
            });
        });
    }

    // Создаем индекс по идентификатору транзакции
    collection.createIndex({'hash': 1}, {unique: true}, function (err, result) {
        if (err) return onError(err);

        // Start processing
        nextChunk(FIRST_CONTRACT_BLOCK, CHUNK_BLOCK_OFFSET);
    });
});
