const elasticsearch = require('elasticsearch');
const async = require('async');
const getDatabase = require('../utils/getDatabase');
const ObjectID = require('mongodb').ObjectID;

const wallets = require('../wallets.json');

const client = new elasticsearch.Client({
    host: [
        {
            host: process.env.ES_HOST,
            auth: process.env.ES_AUTH,
            protocol: 'https',
            port: 9243
        }
    ],
    log: 'trace'
});


async.waterfall([

    // //
    // // Удаляем индексы
    // //
    // function (next) {
    //
    //     async.waterfall([
    //
    //         // Удаляем индекс токенов
    //         function (next2) {
    //             client.indices.delete({
    //                 index: 'tokens',
    //                 ignore: [404]
    //             }, next2);
    //         },
    //
    //         // Удаляем индекс кошельков
    //         function (next2) {
    //             client.indices.delete({
    //                 index: 'wallets',
    //                 ignore: [404]
    //             }, next2);
    //         }
    //     ], next);
    // },

    //
    // Загружаем token_trades
    //
    function (next) {

        getDatabase(function (err, db) {
            if (err) return next(err);

            const collection = db.collection('statistic.tokens_trades');
            collection.find({_id: {$gte:  ObjectID("5a92c10fbfe2fd0760369543")}}).toArray(function (err, token_trades) {
                if (err) return next(err);

                async.eachSeries(token_trades, function (token_trade, callback) {
                    delete token_trade._id;

                    client.index({
                        index: 'etherdelta_tokens',
                        id: token_trade.tx,
                        type: 'token_trades',
                        body: token_trade
                    }, callback);
                }, next);
            });
        });
    },

    //
    // Загружаем кошельки
    //
    // function (next) {
    //
    //     getDatabase(function (err, db) {
    //         if (err) return next(err);
    //
    //         const collection = db.collection('statistic.tokens_trades');
    //         collection.find({_id: {$gte:  ObjectID("5a92c10fbfe2fd0760369543")}}).toArray(function (err, token_trades) {
    //             if (err) return next(err);
    //
    //             async.eachSeries(token_trades, function (token_trade, callback) {
    //                 delete token_trade._id;
    //
    //                 client.index({
    //                     index: 'etherdelta_tokens',
    //                     id: token_trade.tx,
    //                     type: 'token_trades',
    //                     body: token_trade
    //                 }, callback);
    //             }, next);
    //         });
    //     });
    // },

    //
    // Загружаем кошельки
    //
    function (next) {
        async.eachSeries(wallets, function (wallet, callback) {

            client.index({
                index: 'etherdelta_wallets',
                id: wallet.wallet_address,
                type: 'wallets',
                body: wallet
            }, function (err) {
                process.nextTick(callback, err);
            });
        }, next);
    }

], function (err) {
    if (err) throw err;
    console.log('Статистика успешно загружена!');
    process.exit(0);
});