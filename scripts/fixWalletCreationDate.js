const async = require('async');
const getDatabase = require('../utils/getDatabase');
const getWalletCreationDate = require('../utils/getWalletCreationDate');

function onError(err) {
    console.error(err);
    process.exit(1);
}

function onSuccess() {
    console.log('Дата создания успешно исправлена');
    process.exit(0);
}

getDatabase(function (err, db) {
    if (err) throw err;

    async.parallel([

        // Обновляем дату создания кошелька в кошельках
        function (callback) {
            const collection = db.collection('statistic.wallets');

            collection.find({wallet_created_at: '1970-01-01T00:00:00.000Z'}, {_id: 1, wallet_address: 1}).toArray(function (err, wallets) {
                if (err) return callback(err);

                async.eachSeries(wallets, function (wallet, nextEachCallback) {
                    console.log(`Обновляем кошелек с адресом ${wallet['wallet_address']}`);

                    // Получаем дату создания кошелька и обновляем документ
                    getWalletCreationDate(wallet['wallet_address'], function (err, wallet_created_at) {
                        if (err) return nextEachCallback(err);

                        // Пересодим к следующей итерации
                        process.nextTick(nextEachCallback, null);

                        collection.updateOne(
                            {_id: wallet['_id']},
                            {
                                $set: {
                                    wallet_created_at: new Date(wallet_created_at * 1000).toISOString()
                                }
                            },
                            function (err, result) {
                                if (err) return nextEachCallback(err);

                                if (result.updatedCount !== 1) {
                                    return nextEachCallback(new Error(`Не удалось обновить кошелек с адресом ${wallet['wallet_address']}`));
                                }

                                // Пересодим к следующей итерации
                                process.nextTick(nextEachCallback, null);
                        });
                    });
                }, callback);
            });
        },

        // Обновляем дату создания кошелька в сделках
        function (callback) {
            const collection = db.collection('statistic.tokens_trades');

            collection.find({wallet_created_at: '1970-01-01T00:00:00.000Z'}, {_id: 1, wallet_address: 1}).toArray(function (err, tokens_trades) {
                if (err) return callback(err);

                async.eachSeries(tokens_trades, function (token_trade, nextEachCallback) {
                    console.log(`Обновляем сделку с кошельком ${token_trade['wallet_address']}`);

                    // Получаем дату создания кошелька и обновляем документ
                    getWalletCreationDate(token_trade['wallet_address'], function (err, wallet_created_at) {
                        if (err) return nextEachCallback(err);

                        // Пересодим к следующей итерации
                        process.nextTick(nextEachCallback, null);

                        collection.updateOne(
                            {_id: token_trade['_id']},
                            {
                                $set: {
                                    wallet_created_at: new Date(wallet_created_at * 1000).toISOString()
                                }
                            },
                            function (err, result) {
                                if (err) return nextEachCallback(err);

                                if (result.updatedCount !== 1) {
                                    return nextEachCallback(new Error(`Не удалось обновить сделку ${token_trade['wallet_address']}`));
                                }

                                // Пересодим к следующей итерации
                                process.nextTick(nextEachCallback, null);
                            });
                    });
                }, callback);
            });
        }

    ], function (err) {
        if (err) return onError(err);
        return onSuccess(err);
    });
});
